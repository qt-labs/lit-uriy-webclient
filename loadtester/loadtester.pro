#-------------------------------------------------
#
# Project created by QtCreator 2009-08-18T15:16:56
#
#-------------------------------------------------

QT       += webkit

TARGET = loadtester
TEMPLATE = app
CONFIG += release


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui
