#include <QtGui>
#include <webclient.h>

class ButtonResponder : public QObject
{
Q_OBJECT
public slots:
    void pressed()
    {
        qDebug() << "pressed";
    }

    void released()
    {
        qDebug() << "released";        
    }
};


int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    QPushButton *pushButton = new QPushButton("Hello Web");

    ButtonResponder responder;
    QObject::connect(pushButton, SIGNAL(pressed()), &responder, SLOT(pressed()));
    QObject::connect(pushButton, SIGNAL(released()), &responder, SLOT(released()));

    WebClient server;
    server.setRootWidget(pushButton);
    pushButton->show();

    return app.exec();
}

#include "main.moc"

