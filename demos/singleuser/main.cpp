#include <QtGui>
#include <QUiLoader>
#include <webclient.h>

QWidget *loadui(const QString &filename)
{
     QUiLoader loader;
     QFile file(filename);
     file.open(QFile::ReadOnly);
     return loader.load(&file, 0);
}

int main(int argc, char **argv)
{
    QApplication app(argc, argv);

 //   QWidget *myWidget = loadui("helpdialog.ui");
    QWidget *myWidget = loadui("widgets.ui");

    WebClient webclient;
    webclient.setPort(2020);
    webclient.setRootWidget(myWidget);
    myWidget->show();

    return app.exec();
}

