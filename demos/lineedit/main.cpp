#include <QtGui>
#include <webclient.h>

WebClient *webClient;

class QRcServer : public QObject
{
    Q_OBJECT
    public slots:
    void instantiateUi(QWidget **root, Session *session)
    {
        Q_UNUSED(session);

        QMdiArea *mdiArea = new QMdiArea;
        webClient->setWidgetHint(mdiArea->viewport(), WebClient::StaticWidget);
        *root = mdiArea;

        QWidget *mainWidget = new QWidget();
        QMdiSubWindow *mdiSub = mdiArea->addSubWindow(mainWidget);
        webClient->setWidgetHint(mdiSub, WebClient::StaticWidget);
       
        QVBoxLayout *mainLayout = new QVBoxLayout();
        
        QString text = "<b>Line edits example: </b> The content of the native " 
        "line edits is synchronized server-side";
        mainLayout->addWidget(new QLabel(text));
        
        QHBoxLayout *layout = new QHBoxLayout();
        
        QLineEdit *lineEdit1 = new QLineEdit();
        layout->addWidget(lineEdit1);
        
        QLineEdit *lineEdit2 = new QLineEdit();
        layout->addWidget(lineEdit2);
        
        QObject::connect(lineEdit1, SIGNAL(textEdited(const QString &)), lineEdit2, SLOT(setText(const QString &)));
        QObject::connect(lineEdit2, SIGNAL(textEdited(const QString &)), lineEdit1, SLOT(setText(const QString &)));
        
        QPushButton *pushButton = new QPushButton("Clear Text");
        layout->addWidget(pushButton);
        QObject::connect(pushButton, SIGNAL(clicked()), lineEdit1, SLOT(clear()));
        QObject::connect(pushButton, SIGNAL(clicked()), lineEdit2, SLOT(clear()));
        
        mainLayout->addLayout(layout);
        mainWidget->setLayout(mainLayout);
    }
};
int main(int argc, char **argv)
{
    QApplication app(argc, argv);
    
    
    webClient = new WebClient;
    //    webClient.setActiveSessionLimit(100);
    
    QRcServer qrcServer;
    
    QRcServer object;
    QObject::connect(webClient, SIGNAL(newSession(QWidget **, Session *)),
                     &qrcServer, SLOT(instantiateUi(QWidget **, Session *)));
    
    return app.exec();
}

#include "main.moc"


