#include <QtGui>
#include <QUiLoader>
#include <webclient.h>

QWidget *loadui(const QString &filename)
{
    QUiLoader loader;
    QFile file(filename);
    bool ok = file.open(QFile::ReadOnly);
    if (!ok)
        qWarning("WARNING: Could not open file %s", filename.toLocal8Bit().constData());
    return loader.load(&file, 0);
}

void addSubWindow(QMdiArea *area, const QString &file)
{
    area->addSubWindow(loadui(file));
}

int main(int argc, char **argv)
{
    QApplication app(argc, argv);
    //app.setStyle("plastique");    

    QMdiArea *area = new QMdiArea();
    addSubWindow(area, "buttons.ui");
    addSubWindow(area, "widgets.ui");
    addSubWindow(area, "helpdialog.ui");
    addSubWindow(area, "gdboptionpage.ui");

 //   area->addSubWindow(new QWidget);
    WebClient server;
    server.setRootWidget(area);
  //  area->show();    
            
    return app.exec();
}

