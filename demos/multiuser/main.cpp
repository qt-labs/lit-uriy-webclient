#include <QtGui>
#include <QUiLoader>
#include <webclient.h>

QWidget *loadui(const QString &filename)
{
    QUiLoader loader;
    QFile file(filename);
    bool ok = file.open(QFile::ReadOnly);
    if (!ok)
        qWarning("WARNING: Could not open file %s", filename.toLocal8Bit().constData());
    return loader.load(&file, 0);
}

class MyApplicationObject : public QObject
{
Q_OBJECT
public slots:
    void instantiateUi(QWidget **root, Session *session)
    {
        Q_UNUSED(session);
        *root = loadui("widgets.ui");
    }
};

int main(int argc, char **argv)
{
    QApplication app(argc, argv);
    app.setStyle("cleanlooks");

    WebClient webclient;
    
    MyApplicationObject object;
    QObject::connect(&webclient, SIGNAL(newSession(QWidget **, Session *)),
                     &object, SLOT(instantiateUi(QWidget **, Session *)));

    return app.exec();
}

#include "main.moc"
