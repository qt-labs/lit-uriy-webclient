/****************************************************************************
 **
 ** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
 ** Contact: Qt Software Information (qt-info@nokia.com)
 **
 ** This file is part of the WebClient project on Trolltech Labs.
 **
 ** This file may be used under the terms of the GNU General Public
 ** License version 2.0 or 3.0 as published by the Free Software Foundation
 ** and appearing in the file LICENSE.GPL included in the packaging of
 ** this file.  Please review the following information to ensure GNU
 ** General Public Licensing requirements will be met:
 ** http://www.fsf.org/licensing/licenses/info/GPLv2.html and
 ** http://www.gnu.org/copyleft/gpl.html.
 **
 ** If you are unsure which license is appropriate for your use, please
 ** contact the sales department at qt-sales@nokia.com.
 **
 ** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
 ** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 **
 ****************************************************************************/

#include "webclient.h"
#include "webclientserver.h"
#include "sessionserver.h"

WebClient::WebClient()
{
    server = 0;
    setPort(1818);
}

WebClient::~WebClient()
{
    delete server;
}


void WebClient::setPort(quint16 port)
{
    delete server;
    server = new Server(port);

    connect(server, SIGNAL(sessionBegin(Session *)),
              this, SLOT(newSession_internal(Session *)));

}

void WebClient::setWidgetHint(QWidget *widget, WidgetHint hint)
{
    server->widgetHints[widget].insert(hint);
}

void WebClient::setRootWidget(QWidget *widget)
{
    globalRootWidget = widget;
}

void WebClient::setActiveSessionLimit(int sessionLimit)
{
    this->server->activeSessionLimit = sessionLimit;
}

void WebClient::setActiveSessionLimitHtml(const QString &html)
{
    this->server->activeSessionLimitHtml = html.toUtf8();
}

void WebClient::setInactiveSessionTimeout(int seconds)
{
    this->server->inactiveSessionTimeout = seconds;
}

void WebClient::newSession_internal(Session *session)
{
    QWidget *rootWidget = 0;
    emit newSession(&rootWidget, session);

    if (rootWidget == 0)
        rootWidget = globalRootWidget;

    if (rootWidget == 0) {
        qWarning("rootWidget is NULL.");
        return;
    }
    
    new SessionServer(rootWidget, session, server);
}
