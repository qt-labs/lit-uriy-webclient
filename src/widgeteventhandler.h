/****************************************************************************
 **
 ** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
 ** Contact: Qt Software Information (qt-info@nokia.com)
 **
 ** This file is part of the WebClient project on Trolltech Labs.
 **
 ** This file may be used under the terms of the GNU General Public
 ** License version 2.0 or 3.0 as published by the Free Software Foundation
 ** and appearing in the file LICENSE.GPL included in the packaging of
 ** this file.  Please review the following information to ensure GNU
 ** General Public Licensing requirements will be met:
 ** http://www.fsf.org/licensing/licenses/info/GPLv2.html and
 ** http://www.gnu.org/copyleft/gpl.html.
 **
 ** If you are unsure which license is appropriate for your use, please
 ** contact the sales department at qt-sales@nokia.com.
 **
 ** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
 ** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 **
 ****************************************************************************/

#ifndef WIDGETHANDLER_H
#define WIDGETHANDLER_H

#include <QtGui>
#include <eventqueue.h>
#include <webclientserver.h>

class Server;
class WidgetEventHandler : public QObject
{
Q_OBJECT
public:
    WidgetEventHandler(QObject *parent, Server * server);
    void setRootWidget(QWidget *root);
    bool shouldProcessChildWidgets(QWidget *widget);
    void recursivelyInstallEventHandler(QWidget *widget);
    void setSession(Session *session);
    Session *session();
    void handleRequest(HttpRequest *request, HttpResponse *response);
    void recursivelyAddShow(QWidget *root);
    void recursivelyAddUpdate(QWidget *root);
protected slots:
    void updatePendingWidgets();
    void textChange();
    void widgetDeleted();

protected:
    quintptr idForWidget(QWidget *widget);
    QWidget *widgetForId(quintptr id);
    QSet<quintptr> liveWidgets;

    bool handleJsonMessage(const QByteArray &message);
    void handleMousePress(const QByteArray &message);
    void handleKeyPress(const QByteArray &message);
    void handleTextUpdate(json_object* object);
    void handlePositionUpdate(json_object *object);
    void addPendingUpdate(QWidget* widget, const QRect &rect);
    bool eventFilter(QObject *object, QEvent *event);
    void widgetPaint(QWidget *widget, const QRect &updateRect);
    QRect globalGeometry(QWidget *widget);
    
    void recursivelyAddHide(QWidget *root);
    void addShowEvent(QWidget *widget);


public: //private:
    EventQueue events;
    QHash<QWidget *, QRect> pendingUpdates;
    bool graphicsWidget;
    bool grabbing;
    QWidget *rootWidget;
    QWidget *focusWidget; // hack hack
    QSet<QObject*> disableUpdates;
    QSet<QObject *> staticWidgets;
    Server *server;
};

#endif
