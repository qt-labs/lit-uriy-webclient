/****************************************************************************
 **
 ** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
 ** Contact: Qt Software Information (qt-info@nokia.com)
 **
 ** This file is part of the WebClient project on Trolltech Labs.
 **
 ** This file may be used under the terms of the GNU General Public
 ** License version 2.0 or 3.0 as published by the Free Software Foundation
 ** and appearing in the file LICENSE.GPL included in the packaging of
 ** this file.  Please review the following information to ensure GNU
 ** General Public Licensing requirements will be met:
 ** http://www.fsf.org/licensing/licenses/info/GPLv2.html and
 ** http://www.gnu.org/copyleft/gpl.html.
 **
 ** If you are unsure which license is appropriate for your use, please
 ** contact the sales department at qt-sales@nokia.com.
 **
 ** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
 ** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 **
 ****************************************************************************/

#ifndef QT_WEBCLIENT_H
#define QT_WEBCLIENT_H

#include <QtGui>
#include "webclientserver.h"

#ifdef QT_WEBCLIENT_DEBUG
#define DEBUG qDebug()
#else
#define DEBUG if (0) qDebug()
#endif

class Server;
class Session;
class WebClient : public QObject
{
Q_OBJECT
public:
    WebClient();
    ~WebClient();
    void setRootWidget(QWidget *widget);
    void setPort(quint16 port);

    void setActiveSessionLimit(int sessionLimit);
    void setActiveSessionLimitHtml(const QString &html);
    void setInactiveSessionTimeout(int seconds);

    enum WidgetHint { StaticWidget, NoDisplayWidget };
    void setWidgetHint(QWidget *widget, WidgetHint hint);

signals:
    void newSession(QWidget **rootWidget, Session *session);
private slots:
    void newSession_internal(Session *session);
private:
    Server *server;
    QWidget *globalRootWidget;
};



#endif QT_WEBCLIENT_H


