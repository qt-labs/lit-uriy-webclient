/****************************************************************************
 **
 ** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
 ** Contact: Qt Software Information (qt-info@nokia.com)
 **
 ** This file is part of the WebClient project on Trolltech Labs.
 **
 ** This file may be used under the terms of the GNU General Public
 ** License version 2.0 or 3.0 as published by the Free Software Foundation
 ** and appearing in the file LICENSE.GPL included in the packaging of
 ** this file.  Please review the following information to ensure GNU
 ** General Public Licensing requirements will be met:
 ** http://www.fsf.org/licensing/licenses/info/GPLv2.html and
 ** http://www.gnu.org/copyleft/gpl.html.
 **
 ** If you are unsure which license is appropriate for your use, please
 ** contact the sales department at qt-sales@nokia.com.
 **
 ** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
 ** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 **
 ****************************************************************************/

var topZ = 1;
var startX;
var startY;
startX = 100;
startY = 100;
nextID = 1;

var images = new Array;

function elmentId(pointer)
{
    return "divWidget" + pointer;
}

function createWidgetElement(pointer)
{
    var element = document.createElement("img");
    element.id = this.elmentId(pointer);
    element["pointer"] = pointer;
    element.className = "divWidget";
    element.imageLoaded = false;
    hideElement(element);
    document.body.appendChild(element);

    dojo.connect(element, 'onmousedown', this, sendMousePressedPreventDefault);
    dojo.connect(element, 'onmouseup', this, sendMouseReleased);
    dojo.connect(element, 'ondblclick', this, sendMouseDoubleClick);
    dojo.connect(element, 'onkeypress', this, sendKeyPress);
    dojo.connect(element, 'onload', this, imageLoaded);
    return element;
}

function createInputElement(pointer, type)
{
    var inputElement = document.createElement("input");
    inputElement.id = this.elmentId(pointer);
    inputElement.pointer = pointer;
    inputElement.type = type;
    inputElement.className = "nativeWidget";
    inputElement.style.visibility = "visible";
    document.body.appendChild(inputElement);

    return inputElement;
}

function createTextInputElement(pointer)
{
    var inputElement = createInputElement(pointer, "text")
    dojo.connect(inputElement, 'onkeypress', this, inputElementKeyPress);
    dojo.connect(inputElement, 'onkeyup', this, inputElementKeyRelease);
    return inputElement;
}

function createButtonInputElement(pointer)
{
    var element = createInputElement(pointer, "button");
    dojo.connect(element, 'onmousedown', this, sendButtonPressed);
    dojo.connect(element, 'onmouseup', this, sendButtonReleased);
    element.style.fontSize = 11;
    return element;
}

function createTextareaElement(pointer)
{
    var inputElement = document.createElement("textarea");
    inputElement.id = this.elmentId(pointer);
    inputElement.pointer = pointer;
    inputElement.className = "nativeWidget";
    inputElement.style.visibility = "visible";
    document.body.appendChild(inputElement);

    dojo.connect(inputElement, 'onkeypress', this, inputElementKeyPress);
    dojo.connect(inputElement, 'onkeyup', this, inputElementKeyRelease);

    return inputElement;
}

function createLabelElement(pointer)
{
    var spanElement = document.createElement("span");
    spanElement.id = this.elmentId(pointer);
    spanElement.pointer = pointer;
    spanElement.className = "nativeWidget";
    spanElement.style.visibility = "visible";

    // roughly position and size the text to match the
    // default Qt label style.
    spanElement.style.marginTop = 4;
    spanElement.style.fontSize = 11;
    spanElement.style.fontFamily = "sans-serif";

    document.body.appendChild(spanElement);

    return spanElement;
}

function createStructureElement()
{
    var element = document.createElement("div");
    element.className = "divStructure";
    element.style.visibility = "visible";
    document.body.appendChild(element);
    return element;
}

function createNoopElement()
{
    var element = document.createElement("div");
    element.className = "divNoop";
    element.style.visibility = "hidden";
    document.body.appendChild(element);
    return element;
}

function createElement(widgetType, id)
{
    //console.log("x createElement src: " + this.baseUrl + " " + widgetType + " id" + id);

    var element;
    if (widgetType == "lineedit") {
        element = this.createTextInputElement(id);
    } else if (widgetType == "textedit") {
        element = this.createTextareaElement(id);
        element.readOnly = true;
    } else if (widgetType == "pusbutton") {
        element = this.createButtonInputElement(id);
    } else if (widgetType == "label") {
        element = this.createLabelElement(id);
    } else if (widgetType == "midisubwindow") {
//        console.log("midisubwindow")
        element = this.createWidgetElement(id);

        dojo.connect(element, "ondragstart", dojo, "stopEvent");
        dojo.connect(element, 'onmousedown', this, dragBegin);
    } else if (widgetType == "skippedwidget") {
        element = createNoopElement(id);
    } else {
        element = this.createWidgetElement(id);
    }

    element.style.zIndex = 1;
    var structureElement = this.createStructureElement();
    structureElement.style.zIndex = 0;
    element["structureElement"] = structureElement;
    structureElement.appendChild(element);
    return element;
}

function showElement(element)
{
    // delay showing of generic widgets until the image has ben loaded,
    if (element.className == "divWidget") {
        if (element.imageLoaded)
            element.style.visibility = "visible";
    } else {
        element.style.visibility = "visible";
    }
}

function hideElement(element)
{
    element.style.visibility = "hidden";
}

function imageLoaded(event)
{
    event.target.imageLoaded = true;
    event.target.style.visibility = "visible";
}

function updateText(element, text)
{
//    console.log("update text" + text + " " + element.nodeName);
    if (element.nodeName == "SPAN")
        element.innerHTML = text;
    else
        element.value = text;
}

function setElementRelativePosition(contentElement, x, y)
{
//    console.log("set element relative posisiton " + x + " " + y);
    var element = contentElement.structureElement;
    element.style.left = x;
    element.style.top = y;
}

function moveElement(contentElement, dx, dy)
{
    var element = contentElement.structureElement;
    element.style.left = parseInt(element.style.left) + dx;
    element.style.top = parseInt(element.style.top) + dy;
//    console.log("move element " + dx + " " + dy + element.style.left);
}

function sendMoveMessage(element)
{
    var structureElement = element.structureElement;
//    console.log("element.structureElement" + structureElement + " " + element.pointer);
    var message = this.jsonUrl + JSON.stringify({ "type" : "positionupdate", "id" : element.pointer,
                                                  "x" : structureElement.style.left, "y" : structureElement.style.top });
    this.request(message);
}

function printWidgetArray(array)
{
    for(key in array) {
        console.log(array[key].id);
    }
}

function removeItem(array, item)
{
//    console.log("array remove " + item.id);
//    printWidgetArray(array);
    for(key in array) {
        if (array[key] == item)
            array.splice(key, 1);
    }
//    console.log("done: ");
//    printWidgetArray(array);

}

function addUnique(array, item)
{
    for(key in array) {
        if (array[key] == item)
            return;
    }
    array.push(item);
}

function setTopLevel(element, enable)
{
 //   console.log("Set Top level " + element + " " + enable );
    if (enable) {
        element.style.position = "relative";
        this.appendChild(element);
    } else {
        element.style.position = "absolute";
    }
}

function isTopLevel(element)
{
    return (element.structureElement.style.position == "relative");
}

function eventHandler(text)
{
    if (text == "") {
        return;
    }

    var array = eval("(" + text + ")");
    
//    console.log("eventHandler src: " + this.baseUrl + "text" + text);

    for (key in array)
    {
        var event = array[key];
        var type = array[key].type
        var widgetType = array[key].widgetType;
        var widget = array[key].id;
        var id = this.elmentId(widget);
        var element = document.getElementById(id);

//        console.log("event " + event.type);

        if (!element)
            element = this.createElement(widgetType, widget);
    
        //        alert("key" + key + "type" + type);
        if (type == "update") {
//            alert("update" + widget);

            var source;

            if (array[key].imagehash) {
                var imagehash = array[key].imagehash;
                source = this.jsonUrl + JSON.stringify({ "type" : "img-static", "id" : widget, "imagehash" : imagehash });
            } else {
                source = this.jsonUrl + JSON.stringify({ "type" : "img", "id" : widget, "rand" : Math.random()});
            }

            element.src = source;
        } else if (type == "geometry") {
//            console.log("geometry x " + event.x + " y " + event.y + " w " + event.w + " h " + event.h);
            this.setElementRelativePosition(element, event.x, event.y)
            element.style.width = event.w;
            element.style.height = event.h;

            // resize the webclient div as well when resizing the top-level widget.
            if (isTopLevel(element)) {
                this.style.width = event.w;
                this.style.height = event.h;
            }

        } else if (type == "hide") {
            hideElement(element);
        } else if (type == "show") {
            showElement(element);
            element.qtobjectName = event.objectName;
            element.qtclassName = event.className;
            element.structureElement.qtobjectName = event.objectName;
            element.structureElement.qtclassName = event.className;
        } else if (type == "parentChange") {
     //       alert("parentChange");
            var parentPointer = array[key].parent;
            var parentId = this.elmentId(parentPointer);
            var parent = document.getElementById(parentId);
            var child = element;

       //     console.log("parentChange parent " + parentId + " child " + id);

            this.setTopLevel(element.structureElement, parentPointer == 0);
            if (parent && parent.structureElement && element.structureElement) {
                parent.structureElement.appendChild(element.structureElement);
                element.structureElement.style.zIndex = 2;
            }
        } else if (type == "textUpdate") {
            updateText(element, event.text);
        }
    }
}

function sendMouseEventReq(description, pos) {
//    console.log("mouse event req");
    this.request(this.baseUrl +  description + "-" + pos.x + "-" + pos.y, eventHandler);
}

function calculateMousePos(container, event)
{
    var pos = dojo.coords(container);
    pos.x = event.pageX - pos.x;
    pos.y = event.pageY - pos.y;
    return pos;
}

function sendMousePressedPreventDefault(event) {
    event.preventDefault();
    this.sendMousePressed(event)
}

function sendMousePressed(event) {
    this.sendMouseEventReq("mousepress", calculateMousePos(this, event));
}

function sendMouseReleased(event) {
    this.sendMouseEventReq("mouserelease", calculateMousePos(this, event));
}

function sendButtonPressed(event) {
    var pos = new Object;
    pos.x = event.pageX;
    pos.y = event.pageY;
    this.sendMouseEventReq("mousepress", pos);
}

function sendButtonReleased(event) {
    var pos = new Object;
    pos.x = event.pageX;
    pos.y = event.pageY;
    this.sendMouseEventReq("mouserelease", pos);
}

function sendMouseDoubleClick(event) {
    this.sendMouseEventReq("mousedoubleclick", calculateMousePos(this, event));
}

function sendKeyPress(event) {
//    console.log("sendKeyPresst req");

    if (event.charCode)
        this.request(this.baseUrl +  "keypress-" + event.charCode, eventHandler);
    else
        this.request(this.baseUrl +  "keypress-" + event.keyCode, eventHandler);
}

function inputElementKeyPress(event)
{
//    console.log("press" + event.charCode + " " + event.keyCode);
    if (event.keyCode == 13) // enter, return
        this.sendInputElementTextUpdate(event)
//    if (event.keyCode == 8) // backspace
//        sendInputElementTextUpdate(event)
}

function inputElementKeyRelease(event)
{
//    console.log("reslese " + event.charCode + " " + event.keyCode);
    if (event.keyCode == 13) // enter, return
        return;
    this.sendInputElementTextUpdate(event);
}


function sendInputElementTextUpdate(event)
{
    var id = event.target.pointer;
    var text;
    if (event.keyCode == 13) {
        text = "enter!"; // FIXME! :)
        event.target.value = "";
    } else {
        text = event.target.value;
        text  = text.replace(/%/g, " "); // don't crash the server. (### wrong fix)
        text  = text.replace(/#/g, " ");
    }
    var source = this.jsonUrl + JSON.stringify({ "type" : "textupdate", "id" : id, "text" : text });
    this.request(source);
}

function getElementsByClass(searchClass,node,tag) {
	var classElements = new Array();
	if ( node == null )
		node = document;
	if ( tag == null )
		tag = '*';
	var els = node.getElementsByTagName(tag);
	var elsLen = els.length;
    for (i = 0, j = 0; i < elsLen; i++) {
		if (els[i].className == searchClass ) {
			classElements[j] = els[i];
			j++;
		}
	}
	return classElements;
}

var webclientObjectCounter = 0;

    function dragBegin (event) {
        event = event ? event : window.event;
        event.preventDefault();

        var element = event.target.structureElement;
        if (isNaN(parseInt(element.style.left))) { element.style.left = '0px'; }
        if (isNaN(parseInt(element.style.top))) { element.style.top = '0px'; }

      //  console.log(element.parentNode);

        ++this.globalZIndex;
        element.style.zIndex = this.globalZIndex; // Special case adapted to the QMdiArea hiearchy.

        var x = parseInt(element.style.left);
        var y = parseInt(element.style.top);

        this.mouseX = event.clientX;
        this.mouseY = event.clientY;
        this.dragHandler = dojo.connect(document, 'onmousemove', this, drag);
        this.dragEndHandler = dojo.connect(document, 'onmouseup', this, dragEnd);
        this.dragElement = event.target;

        return false;
    }

    function drag(event) {
        var element = this.dragElement.structureElement;
    //    console.log(this.dragElement + this.dragElement.structureElement)
    //    console.log("Drag " + this + " " + this.dragElement + " " + element)
        var x = parseInt(element.style.left);
        var y = parseInt(element.style.top);

        event = event ? event : window.event;

        var dx = event.clientX - this.mouseX;
        var dy = event.clientY - this.mouseY;

        this.moveElement(this.dragElement, dx, dy);

        this.mouseX = event.clientX;
        this.mouseY = event.clientY;

        return false;
    }

    function dragEnd(event) {
        this.sendMoveMessage(this.dragElement);
        dojo.disconnect(this.dragHandler);
        dojo.disconnect(this.dragEndHandler);
    }


function setUpWebClientObject(webclientObject)
{
    // Set the base url, this tells QWebClient where to
    // "phone home". Use the "src" attribute on the webclient
    // div (set in the html code), or window.location host if
    // not set.
    if (webclientObject.attributes["src"])
        webclientObject.baseUrl = webclientObject.attributes["src"].value + "/";
    else
        webclientObject.baseUrl = window.location.host + "/";

    if (webclientObject.baseUrl.indexOf("http://") == -1)
        webclientObject.baseUrl = "http://" + webclientObject.baseUrl;

    sessionHandlerSetupObject(webclientObject);

    webclientObject.uid = webclientObjectCounter++;
    webclientObject.contentUrl = webclientObject.baseUrl +  "content";
    webclientObject.jsonUrl = webclientObject.baseUrl +  "json";

    
    // attach the functions to the webclient object.
    // ...surely there is a more ideomatic way :)
    webclientObject.elmentId = elmentId
    webclientObject.createWidgetElement = createWidgetElement
    webclientObject.createInputElement = createInputElement
    webclientObject.createLabelElement = createLabelElement
    webclientObject.createTextareaElement = createTextareaElement
    webclientObject.createTextInputElement = createTextInputElement;
    webclientObject.createButtonInputElement = createButtonInputElement;
    webclientObject.createElement = createElement
    webclientObject.setElementRelativePosition = setElementRelativePosition
    webclientObject.moveElement = moveElement
    webclientObject.sendMoveMessage = sendMoveMessage
    webclientObject.eventHandler = eventHandler;
    webclientObject.createStructureElement = createStructureElement;
    webclientObject.setTopLevel = setTopLevel;

    webclientObject.sendMouseEventReq = sendMouseEventReq
    webclientObject.sendMousePressed = sendMousePressed
    webclientObject.sendMousePressedPreventDefault = sendMousePressedPreventDefault;
    webclientObject.sendMouseReleased = sendMouseReleased
    webclientObject.sendButtonPressed = sendButtonPressed
    webclientObject.sendButtonReleased = sendButtonReleased
    webclientObject.sendMouseDoubleClick = sendMouseDoubleClick
    webclientObject.sendKeyPress = sendKeyPress
    webclientObject.sendInputElementTextUpdate = sendInputElementTextUpdate
    webclientObject.inputElementKeyPress = inputElementKeyPress
    webclientObject.inputElementKeyRelease = inputElementKeyRelease

    webclientObject.dragBegin = dragBegin;
    webclientObject.drag = drag;
    webclientObject.dragEnd = dragEnd;
    webclientObject.globalZIndex = 1;
//    console.log(webclientObject);
//    console.log(webclientObject.baseUrl);
}

function myload()
{
    var webclients = getElementsByClass("qwebclient");

    for (i = 0; i < webclients.length; i++) {
        var webclientObject = webclients[i]; 
        setUpWebClientObject(webclientObject)
        webclientObject.request(webclientObject.contentUrl);
    }
}

dojo.addOnLoad(myload);
