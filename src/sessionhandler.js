/****************************************************************************
 **
 ** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
 ** Contact: Qt Software Information (qt-info@nokia.com)
 **
 ** This file is part of the WebClient project on Trolltech Labs.
 **
 ** This file may be used under the terms of the GNU General Public
 ** License version 2.0 or 3.0 as published by the Free Software Foundation
 ** and appearing in the file LICENSE.GPL included in the packaging of
 ** this file.  Please review the following information to ensure GNU
 ** General Public Licensing requirements will be met:
 ** http://www.fsf.org/licensing/licenses/info/GPLv2.html and
 ** http://www.gnu.org/copyleft/gpl.html.
 **
 ** If you are unsure which license is appropriate for your use, please
 ** contact the sales department at qt-sales@nokia.com.
 **
 ** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
 ** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 **
 ****************************************************************************/

function request(theurl)
{
//    console.log("send request: " + theurl);
    var webclientObject = this;
//    var argumentsObject = new Object;
//    argumentsObject["foo"] = "bar space";
//    argumentsObject["foo2"] = "bar2";
    dojo.xhrPost( {
        // The following URL must match that used to test the server.
        url: theurl,
        timeout: 0, // Time in milliseconds
//        content : argumentsObject,

        // The LOAD function will be called on a successful response.
        load: function(response, ioArgs) { 
            if (theurl == webclientObject.idleUrl)
                webclientObject.hasIdler = false;
           // 'this' seems to point to the fucntion at this point.
            webclientObject.eventHandler(response);
            if (webclientObject.hasIdler == false) {
                webclientObject.hasIdler = true;
                webclientObject.request(webclientObject.idleUrl);
            }
        },

        // The ERROR function will be called in an error case.
        error: function(response, ioArgs) { 
          console.error("HTTP status code: ", ioArgs.xhr.status); 
          return response;
          }
       });
}

function sessionHandlerSetupObject(webclientObject)
{
    webclientObject.request = request;
    webclientObject.idleUrl = webclientObject.baseUrl + "idle";
    webclientObject.hasIdler = false;
}

