
DEPENDPATH += $$PWD/src
INCLUDEPATH +=$$PWD/src

SOURCES +=  $$PWD/src/widgeteventhandler.cpp $$PWD/src/eventqueue.cpp $$PWD/src/webclientserver.cpp $$PWD/src/webclient.cpp $$PWD/src/sessionserver.cpp
HEADERS += $$PWD/src/webclient.h $$PWD/src/widgeteventhandler.h $$PWD/src/eventqueue.h $$PWD/src/webclientserver.h $$PWD/src/webclient.h $$PWD/src/sessionserver.h
RESOURCES += $$PWD/src/src.qrc

RESOURCES += $$PWD/3rdparty/dojo.qrc

QT += network

CONFIG += console
OBJECTS_DIR = .obj
MOC_DIR = .moc
